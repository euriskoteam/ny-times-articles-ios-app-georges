//
//  ArticlesController.swift
//  TestApp
//
//  Created by Georges Jamous on 3/2/19.
//  Copyright © 2019 Georges Jamous. All rights reserved.
//

import Foundation
import UIKit


class ArticlesController: UITableViewController {
    
    // articles holds a list of articles loaded
    private var articles: [Article] = []
    let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
    var isLoading = false
    var currentPage = 0

    private enum Constants {
        static let CellIdentifier = "Cell"
        static let PaginationOffset: Int = 20
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildUI()
        loadNextArticlePage()
    }

    
    // loads the articles and reloads the table
    // NOTE: pagination simply implemented, more advanced ways are possible
    func loadNextArticlePage(){
        if isLoading {
            return
        }
        
        isLoading = true
        currentPage = currentPage + 1
        self.setStateLoadingArticles()
        
        ArticleApi.shared.fetchArticlesFromServer(page: currentPage, callback: { [weak self] (newArticles, error) in
            self?.setStateShowingArticles()
            self?.isLoading = false
            
            if let error = error {
                self?.showError(error: error)
                return
            }
            
            if let newArticles = newArticles {
                self?.showArticles(articles: newArticles)
            }
        })
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let percentage = scrollView.contentOffset.y / scrollView.contentSize.height
        // load next page when we reach 70% of scroll
        if percentage > 0.7 {
            loadNextArticlePage()
        }
    }
}

// This class acts as a model, kept here for simplicicty
extension ArticlesController {
    
    func setStateLoadingArticles(){
        activityIndicator.startAnimating()
    }
    
    func setStateShowingArticles(){
        activityIndicator.stopAnimating()
    }
    
    func buildUI(){
        self.view.backgroundColor = UIColor.white
        self.title = "NYT Most Viewed"
        
        let rightBarButtonItem: UIBarButtonItem = {
            let barButtonItem = UIBarButtonItem(customView: activityIndicator)
            return barButtonItem
        }()
        self.navigationItem.setRightBarButton(rightBarButtonItem, animated: true)
        
        TestUtils.FlagTableInClass(table: tableView, className: "ArticlesController")
        tableView.register(ArticleTableViewCell.self, forCellReuseIdentifier: Constants.CellIdentifier)
    }
    
    // showError is just a simple way to show the error
    func showError(error: Error){
        let alertController = UIAlertController(
            title: "Something Happened !",
            message: "\(error)",
            preferredStyle: .alert)
        
        let dismissAction = UIAlertAction(title: "Okay", style: .default) { (action:UIAlertAction) in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(dismissAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showArticles(articles: [Article]){
        self.articles.append(contentsOf: articles)
        self.tableView?.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ArticleTableViewCell.Dimension.Height
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selected = articles[indexPath.row]
        self.navigationController?.pushViewController(ArticleDetailsController.init(article: selected), animated: true)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier, for: indexPath) as! ArticleTableViewCell
        cell.article = articles[indexPath.row]
        cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator

        TestUtils.FlagCellInTable(table: tableView, cell: cell, indexPath: indexPath)
        return cell
    }
}
