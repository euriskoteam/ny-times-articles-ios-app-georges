//
//  AppTheme.swift
//  TestApp
//
//  Created by Georges Jamous on 3/2/19.
//  Copyright © 2019 Georges Jamous. All rights reserved.
//

import UIKit

class AppTheme {
    enum Colors {
        static let TextPrimary = UIColor.black
        static let TextSecondary = UIColor.gray
    }
    enum FontSize {
        static let Headline = CGFloat(16.00)
        static let Body = CGFloat(14.00)
        static let Footnote = CGFloat(12.00)
    }
}
