//
//  GetArticlesResponse.swift
//  TestApp
//
//  Created by Georges Jamous on 3/2/19.
//  Copyright © 2019 Georges Jamous. All rights reserved.
//

import Foundation

struct GetArticlesResponse: Codable {
    let status: String
    let num_results: Int
    let results: [Article]
}

