//
//  LibraryApi.swift
//  TestApp
//
//  Created by Georges Jamous on 3/2/19.
//  Copyright © 2019 Georges Jamous. All rights reserved.
//

import Foundation

// ArticleApi abstracts the more complex HTTPClient methods in a usable manner.
final class ArticleApi {
    
    // For the time being, api keys and base url will be stored here,
    // since there is no way to aqcuire an api key.
    // In practice, credentials can/should be stored in keychain service.
    private enum Constants {
        static let BaseUrl = "https://api.nytimes.com/svc/mostpopular/v2/mostviewed/all-sections/7.json"
        static let ApiKey = "Rk6GfVjL9XA3A5ipo7bjr2fNh80CpeA5"
    }
    
    static let shared = ArticleApi()
    
    // getLinkForPage is the private method that geenrate the GET link
    private func getLinkForPage(page: Int) -> String {
        let offset = "offset=\(20*page)"
        return "\(Constants.BaseUrl)?api-key=\(Constants.ApiKey)&\(offset)"
    }
    
    // fetchArticlesFromServer async fetches the list of articles
    func fetchArticlesFromServer(page: Int, callback: @escaping ([Article]?, Error?)->()) {
        let p = page <= 0 ? 1 : page
        let url = getLinkForPage(page: p)
        HTTPClient.shared.getRequest(url) { (response, data, error) in
            guard error == nil else {
                callback(nil, error)
                return
            }
            
            if let data = data {
                do {
                    let r = try JSONDecoder().decode(GetArticlesResponse.self, from: data)
                    callback(r.results, nil)
                } catch _ {
                    callback(nil, HTTPClient.RequestError.DataCorrupted())
                }
                return
            }
            
            callback(nil, HTTPClient.RequestError.DataCorrupted())
        }
    }
}
