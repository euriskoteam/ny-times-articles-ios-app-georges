//
//  SyncUtils.swift
//  TestApp
//
//  Created by Georges Jamous on 3/2/19.
//  Copyright © 2019 Georges Jamous. All rights reserved.
//

import Foundation

final class SyncUtils {
    
    // Synchronized prevents the lock from being modified by another thread
    static func Synchronize(lock: AnyObject, closure: () -> ()) {
        objc_sync_enter(lock)
        closure()
        objc_sync_exit(lock)
    }

}

