//
//  TestUtils.swift
//  TestApp
//
//  Created by Georges Jamous on 3/3/19.
//  Copyright © 2019 Georges Jamous. All rights reserved.
//

import Foundation
import UIKit

final class TestUtils {
    
    static func FlagTableInClass(table: UITableView, className: String) {
        table.accessibilityIdentifier = TableIdentifierForClass(className: className)
    }
    
    static func FlagCellInTable(table: UITableView, cell: UITableViewCell, indexPath: IndexPath) {
        cell.accessibilityIdentifier = "\(table.accessibilityIdentifier ?? "TableView").\(CellIdentifierForId(cellId: cell.reuseIdentifier ?? "Cell")).\(indexPath.row)"
    }
    
    static func TableIdentifierForClass(className: String) -> String {
        return "\(className).TableView"
    }
    
    static func CellIdentifierForId(cellId: String) -> String {
        return "\(cellId)"
    }
}
