//
//  LoggingUtils.swift
//  TestApp
//
//  Created by Georges Jamous on 3/3/19.
//  Copyright © 2019 Georges Jamous. All rights reserved.
//

import Foundation

// Logger is a simple logging class that loggs only in debug mode
// this class could be further enhanced to support multiple logging levels
// as well as storing/uploading logs
// To Show Logs Add '-D DEBUG' flag in Swift Custom Flags
final class Logger {
    private init(){}
    
    static func Info(message:String, function:String = #function) {
        Logger.log(message: "[INFO] \(message)", function: function)
    }
    
    static func Warn(message:String, function:String = #function) {
        Logger.log(message: "[WARN] \(message)", function: function)
    }
    
    static private func log(message:String, function:String = #function) {
        #if DEBUG
        NSLog("%@, %@", function, message)
        #endif
    }
}


